#!/bin/bash

export DHL_GLOBAL_CONFIG="/etc/dhl/global.yaml"
export DHL_REPO_CONFIG="/etc/dhl/repo.yaml"
export DHL_DEPLOYMENT_REPO_CLONE_PATH="/tmp/deployment"
export DHL_DEPLOY_IMAGE_DEPENDENCIES_PATH="$DHL_DEPLOYMENT_REPO_CLONE_PATH/versions.yaml"
export DHL_GIT_AUTHOR_NAME="Dockhand Lite Service Account"
export DHL_GIT_AUTHOR_EMAIL="dhl@boxboat.com"

add_additional_dhl_global_file() {
  [ -z "$1" ] && echo "add_additional_dhl_global_file must be supplied with 1 argument - additional dhl global file path" >&2 && exit 1

  if [ -f "$1" ]; then
    export DHL_GLOBAL_CONFIG="$DHL_GLOBAL_CONFIG:$1"
  fi
}

add_additional_dhl_repo_file() {
  [ -z "$1" ] && echo "add_additional_dhl_repo_file must be supplied with 1 argument - additional dockhand repo file path" >&2 && exit 1
  
  if [ -f "$1" ]; then
    export DHL_REPO_CONFIG="$DHL_REPO_CONFIG:$1"
  fi
}

clone_deployment_repo() {
  [ -z "$DOCKHAND_LITE_DEPLOY_KEY" ] && echo "DOCKHAND_LITE_DEPLOY_KEY must be set" >&2 && exit 1
  [ ! -f "$DOCKHAND_LITE_DEPLOY_KEY" ] && echo "DOCKHAND_LITE_DEPLOY_KEY file must be exist" >&2 && exit 1

  [ -z "$1" ] && echo "clone_deployment_repo must be supplied with 1 argument - deployment project path" >&2 && exit 1

  if [ ! -d $DHL_DEPLOYMENT_REPO_CLONE_PATH ]; then
    (
      export GIT_SSH_COMMAND='ssh -i "'"$DOCKHAND_LITE_DEPLOY_KEY"'" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"'
      git clone "git@gitlab.com:$1.git" $DHL_DEPLOYMENT_REPO_CLONE_PATH
    )
  fi
}

checkout_deployment_repo_deploy_branch() {
  [ -z "$1" ] && echo "checkout_deployment_repo_deploy_branch must be supplied with 1 argument - branch" >&2 && exit 1

  (
    cd $DHL_DEPLOYMENT_REPO_CLONE_PATH
    git checkout "deploy/$1" || git checkout -b "deploy/$1"
  )
}

commit_and_push_deployment_repo() {
  [ -z "$DOCKHAND_LITE_DEPLOY_KEY" ] && echo "DOCKHAND_LITE_DEPLOY_KEY must be set" >&2 && exit 1
  [ ! -f "$DOCKHAND_LITE_DEPLOY_KEY" ] && echo "DOCKHAND_LITE_DEPLOY_KEY file must be exist" >&2 && exit 1

  [ -z "$1" ] && echo "commit_and_push_deployment_repo must be supplied with 1 argument - commit message" >&2 && exit 1

  (
    cd $DHL_DEPLOYMENT_REPO_CLONE_PATH

    export GIT_SSH_COMMAND='ssh -i "'"$DOCKHAND_LITE_DEPLOY_KEY"'" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no"'
    export GIT_AUTHOR_NAME="$GIT_AUTHOR_NAME"
    export GIT_AUTHOR_EMAIL="$GIT_AUTHOR_EMAIL"
    export GIT_COMMITTER_NAME="$GIT_AUTHOR_NAME"
    export GIT_COMMITTER_EMAIL="$GIT_AUTHOR_EMAIL"

    if git commit -am "$1"; then
      git push -u origin "$(git branch --show-current)" || git reset HEAD~1
    fi
  )
}

reset_deployment_repo_branch() {
  cd $DHL_DEPLOYMENT_REPO_CLONE_PATH
  git reset --hard
  git clean -fd
}

write_deploy_image_dependencies() {
  [ -z "$1" ] && echo "list_deploy_image_dependencies must be supplied with 1 argument - deployment" >&2 && exit 1

  cd $DHL_DEPLOYMENT_REPO_CLONE_PATH
  add_additional_dhl_repo_file "dockhand.yaml"

  dhl deploy:list-dependencies \
      "--deployment=$1" \
      -o yaml \
      --outputMap \
      --outputPrefix=images \
    > "$DHL_DEPLOY_IMAGE_DEPENDENCIES_PATH"
}

sync_deployment_versions() {
  [ -z "$1" -o -z "$2" ] && echo "sync_deployment_versions must be supplied with 2 arguments - deployment project path, deployments" >&2 && exit 1

  clone_deployment_repo "$1"
  for deployment in $2; do

    synced="false"
    for i in $(seq 2 2 10); do
      checkout_deployment_repo_deploy_branch "$deployment"
      write_deploy_image_dependencies "$deployment"
      commit_and_push_deployment_repo "update versions" && synced="true" && break

      # deployment update failed
      reset_deployment_repo_branch
      sleep "$i"
    done
    
    if [ "$synced" = "false" ]; then
      echo "failed to sync versions" >&2
      exit 1
    fi
  done
}

# TODO finish this
add_dhl_image_overrides() {
  [ -z "$1" ] && echo "deployment_add_overrides must be supplied with 1 argument - deployment" >&2 && exit 1
  override="false"
  i="0"
  while true; do
    override_event_var="$(eval echo "\$OVERRIDE_EVENT_VAR_$i")"
    override_event_artifact="$(eval echo "\$OVERRIDE_EVENT_ARTIFACT_$i")"
    if [ -n "$override_event_var" ] && [ -n "$override_event_artifact" ]; then
      override_event_version="$(eval echo "\$$override_event_var")"
      if [ -n "$override_event_version" ]; then
        echo "env var override $override_event_var for artifact '$override_event_artifact' set to '$override_event_version'"
        if [ "$override" = "false" ]; then
          echo "deploy:"                                  >  /tmp/dockhand-overrides.yaml
          echo "  deploymentMap:"                         >> /tmp/dockhand-overrides.yaml
          echo "    $1:"                                  >> /tmp/dockhand-overrides.yaml
          echo "      overrides:"                         >> /tmp/dockhand-overrides.yaml
          override="true"
        fi
          echo "        - artifacts:"                     >> /tmp/dockhand-overrides.yaml
          echo "            docker:"                      >> /tmp/dockhand-overrides.yaml
          echo "              - $override_event_artifact" >> /tmp/dockhand-overrides.yaml
          echo "          event: $override_event_version" >> /tmp/dockhand-overrides.yaml
      else
        echo "env var override $override_event_var for artifact '$override_event_artifact' not set"
      fi
    else
      break
    fi
    i=$((i+1))
  done
  if [ "$override" = "true" ]; then
    (
      export DHL_GLOBAL_CONFIG="/etc/dhl/global.yaml"
      export DHL_REPO_CONFIG="/etc/dhl/repo.yaml"
      add_dockhand_repo_file "dockhand.yaml"
      add_dockhand_repo_file /tmp/dockhand-overrides.yaml
      deployment_versions_write "$1"
      echo "deployment overrides versions.yaml:"
      cat versions.yaml
    )
  fi
}