FROM boxboat/dockhand-lite:0.3.0

COPY --chown=node:node /config /etc/dhl
COPY --chown=node:node /scripts /etc/dhl/scripts

USER root:root
RUN curl -SsLo /usr/local/bin/dockcmd \
        "https://storage.googleapis.com/boxops/dockcmd/releases/linux-amd64/v1.8.1/dockcmd" \
    && chmod +x /usr/local/bin/dockcmd

USER node:node

COPY --from=docker:20.10.22 /usr/local/bin/docker /usr/local/bin/docker
COPY --from=boxboat/helm:3.10.3 /usr/local/bin/helm /usr/local/bin/helm
COPY --from=boxboat/kubectl:1.23.13 /usr/local/bin/kubectl /usr/local/bin/kubectl
